import { Icon } from '@blueprintjs/core';
import { useLinks } from '../utils/useLinks';
import { useSelection } from '../utils/useSelection';
import { useTasks } from '../utils/useTasks';
import { Button } from './ui/Button';

type ToolbarProps = {};

export const Toolbar = ({}: ToolbarProps): JSX.Element => {
  const selection = useSelection();
  const { getTask, createTask } = useTasks();
  const { createLink } = useLinks();

  return (
    <div className="border-border text-primary shadow-sidebar fixed bottom-0 left-1/2 flex -translate-x-[50%] items-center gap-2 rounded-[6px] rounded-b-none border bg-white p-2 text-sm">
      <Button
        onClick={() => {
          if (selection.tasks.length !== 1) {
            // createTask();
          } else {
            const parentTask = getTask(selection.tasks[0]);
            if (parentTask) {
              const newTask = createTask(parentTask);
              // createLink({
              //   source: parentTask.id,
              //   target: newTask.id,
              //   type: 'child',
              // });
            }
          }
        }}
      >
        <Icon icon="add" size={12} /> Create new task
      </Button>
      {/* <Button>
        // can use refs to construct
        <Icon icon="new-layers" size={12} /> Materialized task
      </Button> */}
      <Button>
        <Icon icon="new-link" size={12} /> Link
      </Button>
      {/* <Button>
        <Icon icon="data-lineage" size={12} /> New view
      </Button> */}
    </div>
  );
};
