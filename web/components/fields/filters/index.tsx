import { FieldDesc, FieldType, Filter } from 'unravel-common';
import { useWorkspace } from '../../../utils/useWorkspace';
import { PeopleFilter } from './PeopleFilter';
import { SelectFilter } from './SelectFilter';
import { TextFilter } from './TextFilter';

export type FieldFilterProps<T extends FieldType> = {
  descriptor: Extract<FieldDesc, { type: T }>;
  onChange: (filter: any) => void;
};

export type FieldFilterComponent<FT extends FieldType> = (props: FieldFilterProps<FT>) => JSX.Element;

const FieldFilters: {
  [fieldType in FieldType]: FieldFilterComponent<fieldType>;
} = {
  select: SelectFilter,
  people: PeopleFilter,
  text: TextFilter,
} as const;

type FilterRendererProps = {
  filter: Filter['condition'];
};

export const FilterRenderer = ({ filter }: FilterRendererProps): JSX.Element => {
  const { workspace } = useWorkspace();

  const desc = workspace?.fieldDescriptors.find((desc) => desc.id === filter.descId);

  if (!desc) {
    return <div>error</div>;
  }

  const FilterComp = FieldFilters[desc.type];

  return (
    <div>
      <FilterComp
        // @ts-ignore
        descriptor={desc}
        onChange={() => {
          // TODO
        }}
      />
    </div>
  );
};
