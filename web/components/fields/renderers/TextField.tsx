import { useRef } from 'react';
import { FieldRendererProps } from '.';

export const TextField = ({ value, onChange, descriptor, mode }: FieldRendererProps<'text'>): JSX.Element => {
  const val = value[0] || '';

  return (
    <div className="relative inline-block h-full min-w-full max-w-[150px] overflow-hidden whitespace-nowrap px-[1px]">
      <input
        type="text"
        placeholder={mode === 'simple' ? '' : 'Empty'}
        spellCheck={false}
        className="focus-within:border-border absolute left-0 right-0 top-1/2 -translate-y-1/2 rounded border border-transparent leading-tight"
        onChange={(e) => {
          onChange([e.target.value]);
        }}
        value={val}
      />
      {val}
    </div>
  );
};
