import { useRef, useState } from 'react';
import { useClickOutside } from '../../../utils/useClickOutside';
import cx from 'classnames';
import { FieldRendererProps } from '.';

export const SelectField = ({ value, onChange, descriptor, mode }: FieldRendererProps<'select'>): JSX.Element => {
  const [isOpen, setIsOpen] = useState(false);
  const containerRef = useRef<HTMLDivElement>(null);

  useClickOutside(containerRef, () => {
    setIsOpen(false);
  });

  const renderItem = ({ id, label, bgColor, color, border }: typeof descriptor['options'][number]): JSX.Element => {
    return (
      <span
        key={id}
        className="select-none rounded px-1"
        style={{
          color,
          backgroundColor: bgColor,
          borderColor: border,
          borderWidth: '1px',
          borderStyle: 'solid',
        }}
      >
        {label}
      </span>
    );
  };

  return (
    <div className="relative flex h-full min-h-full w-full items-center text-xs" ref={containerRef}>
      <div
        onClick={() => {
          setIsOpen(!isOpen);
        }}
        className={cx('flex min-h-full w-full min-w-full cursor-pointer items-center gap-1 overflow-hidden', {
          'px-1 py-0.5': mode === 'simple',
        })}
      >
        {value.length > 0 ? (
          value.map((valId) => descriptor.options.find((option) => option.id === valId)!).map(renderItem)
        ) : mode === 'simple' ? null : (
          <div className="italic text-gray-400">Empty</div>
        )}
      </div>
      <div
        className={cx(
          'absolute top-full left-0 block whitespace-nowrap rounded border border-gray-300 bg-white shadow',
          {
            hidden: !isOpen,
          },
        )}
        style={{
          zIndex: 2,
        }}
      >
        {descriptor.options.map((option) => {
          const isSelected = value.includes(option.id);
          return (
            <div
              key={option.id}
              onClick={() => {
                if (descriptor.isMultiselect) {
                  if (isSelected) {
                    onChange(value.filter((valId) => valId !== option.id));
                  } else {
                    onChange([...value, option.id]);
                  }
                } else {
                  if (isSelected) {
                    onChange([]);
                  } else {
                    onChange([option.id]);
                  }
                  setIsOpen(false);
                }
              }}
              className={cx('flex cursor-pointer items-center gap-1 py-1 px-1 hover:bg-gray-200')}
            >
              {descriptor.isMultiselect && (
                <input
                  type="checkbox"
                  checked={isSelected}
                  onChange={() => {}}
                  className="pointer-events-none scale-90"
                />
              )}
              {renderItem(option)}
            </div>
          );
        })}
      </div>
    </div>
  );
};
