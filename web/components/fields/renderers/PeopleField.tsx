import { useRef, useState } from 'react';
import { useClickOutside } from '../../../utils/useClickOutside';
import cx from 'classnames';
import { FieldRendererProps } from '.';
import { UserDTO } from 'unravel-common';
import { useCollaboration } from '../../../utils/useCollaboration';

export const PeopleField = ({ value, onChange, descriptor, mode }: FieldRendererProps<'people'>): JSX.Element => {
  const { collaborators } = useCollaboration();
  const [isOpen, setIsOpen] = useState(false);
  const containerRef = useRef<HTMLDivElement>(null);

  useClickOutside(containerRef, () => {
    setIsOpen(false);
  });

  const UserItem = ({
    user: { id, displayName, avatar },
    withName,
  }: {
    user: UserDTO;
    withName?: boolean;
  }): JSX.Element => {
    return (
      <div className="pointer-events-none flex select-none items-center gap-1">
        <div
          key={id}
          className="h-4 w-4 rounded-[50%]"
          style={{
            backgroundPosition: 'center',
            backgroundSize: '100%',
            backgroundImage: `url(${avatar})`,
          }}
          title={displayName}
        ></div>
        {withName && displayName}
      </div>
    );
  };

  return (
    <div className="relative h-full min-h-full w-full text-xs" ref={containerRef}>
      <div
        onClick={() => {
          setIsOpen(!isOpen);
        }}
        className={cx('flex min-h-full w-full min-w-full cursor-pointer gap-1 overflow-hidden', {
          'items-center px-1 py-0.5': mode === 'simple', // justify-center
          'flex-col pt-1': mode === 'full',
        })}
      >
        {value.length > 0 ? (
          value
            .map((valId) => collaborators.find((user) => user.id === valId)!)
            .map((user) => <UserItem key={user.id} user={user} withName={mode === 'full'} />)
        ) : mode === 'simple' ? null : (
          <div className="italic text-gray-400">Empty</div>
        )}
      </div>
      <div
        className={cx(
          'absolute top-full left-0 block whitespace-nowrap rounded border border-gray-300 bg-white shadow',
          {
            hidden: !isOpen,
          },
        )}
        style={{
          zIndex: 2,
        }}
      >
        {collaborators.map((user) => {
          const isSelected = value.includes(user.id);
          return (
            <div
              key={user.id}
              onClick={() => {
                if (descriptor.isMultiselect) {
                  if (isSelected) {
                    onChange(value.filter((valId) => valId !== user.id));
                  } else {
                    onChange([...value, user.id]);
                  }
                } else {
                  if (isSelected) {
                    onChange([]);
                  } else {
                    onChange([user.id]);
                  }
                  setIsOpen(false);
                }
              }}
              className={cx('flex cursor-pointer items-center gap-1 py-1 px-1 hover:bg-gray-200')}
            >
              {descriptor.isMultiselect && (
                <input
                  type="checkbox"
                  checked={isSelected}
                  onChange={() => {}}
                  className="pointer-events-none scale-90"
                />
              )}
              <UserItem key={user.id} user={user} withName />
            </div>
          );
        })}
      </div>
    </div>
  );
};
