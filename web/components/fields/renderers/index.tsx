import { FieldDesc, FieldType, FieldValue } from 'unravel-common';
import { Task, useTasks } from '../../../utils/useTasks';
import { useWorkspace } from '../../../utils/useWorkspace';
import { SelectField } from './SelectField';
import { PeopleField } from './PeopleField';
import { TextField } from './TextField';

export type FieldRendererProps<T extends FieldType> = {
  descriptor: Extract<FieldDesc, { type: T }>;
  value: FieldValue;
  onChange: (value: FieldValue) => void;
  mode: 'simple' | 'simple-no-bd' | 'full';
};

const FieldRenderers: {
  [fieldType in FieldType]: (props: FieldRendererProps<fieldType>) => JSX.Element;
} = {
  select: SelectField,
  people: PeopleField,
  text: TextField,
} as const;

export const FieldRenderer = ({
  task,
  fieldDescriptorId,
  mode = 'simple',
}: {
  task: Task;
  fieldDescriptorId: FieldDesc['id'];
  mode?: 'simple' | 'simple-no-bd' | 'full';
}): JSX.Element => {
  const { workspace } = useWorkspace();
  const { updateTaskField } = useTasks();

  if (!workspace) {
    return <div></div>;
  }

  const desc = workspace.fieldDescriptors.find((desc) => desc.id === fieldDescriptorId);

  if (!desc) {
    return <div></div>;
  }

  const FieldComponent = FieldRenderers[desc.type];

  const value = task.fields.find((field) => field.fieldDescriptorId === fieldDescriptorId)?.value || [];

  return (
    <FieldComponent
      value={value}
      // @ts-ignore
      descriptor={desc}
      mode={mode}
      onChange={(value) => {
        updateTaskField({
          task,
          fieldDescriptorId,
          value,
        });
      }}
    />
  );
};
