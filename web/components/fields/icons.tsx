import { Icon } from '@blueprintjs/core';
import { FieldDesc } from 'unravel-common';

export const FIELD_TYPE_ICON: Record<FieldDesc['type'], JSX.Element> = {
  people: <Icon icon="people" size={12} className="text-icon-gray" />,
  select: <Icon icon="property" size={12} className="text-icon-gray" />,
  text: <Icon icon="text-highlight" size={12} className="text-icon-gray" />,
} as const;
