import { FieldDesc, FieldType } from 'unravel-common';
import { PeopleConfigurator } from './PeopleConfigurator';
import { SelectConfigurator } from './SelectConfigurator';
import { TextConfigurator } from './TextConfigurator';

type FieldConfiguratorProps<T extends FieldType> = {
  descriptor: Extract<FieldDesc, { type: T }>;
  onChange: (desc: Extract<FieldDesc, { type: T }>) => void;
};

export type FieldConfiguratorComponent<FT extends FieldType> = (props: FieldConfiguratorProps<FT>) => JSX.Element;

const FieldConfigurators: {
  [fieldType in FieldType]: FieldConfiguratorComponent<fieldType>;
} = {
  select: SelectConfigurator,
  people: PeopleConfigurator,
  text: TextConfigurator,
};

export const FieldConfigurator = ({
  descriptor,
  onChange,
}: {
  descriptor: FieldDesc;
  onChange: (descriptor: FieldDesc) => void;
}) => {
  const FieldConfiguratorComp = FieldConfigurators[descriptor.type];

  return (
    <FieldConfiguratorComp
      // @ts-ignore
      descriptor={descriptor}
      onChange={onChange}
    />
  );
};
