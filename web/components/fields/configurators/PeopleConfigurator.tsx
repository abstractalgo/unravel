import { FieldConfiguratorComponent } from '.';

export const PeopleConfigurator: FieldConfiguratorComponent<'people'> = ({ descriptor, onChange }) => {
  return (
    <div>
      <div className="flex select-none items-center gap-1 text-xs">
        <input
          type="checkbox"
          checked={descriptor.isMultiselect}
          onChange={() => {
            onChange({
              ...descriptor,
              isMultiselect: !descriptor.isMultiselect,
            });
          }}
        />
        Allow multiple
      </div>
    </div>
  );
};
