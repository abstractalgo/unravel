import { Icon } from '@blueprintjs/core';
import { Fragment } from 'react';
import { Button } from '../../ui/Button';
import { Input } from '../../ui/Input';
import { FieldConfiguratorComponent } from '.';

export const SelectConfigurator: FieldConfiguratorComponent<'select'> = ({ descriptor, onChange }) => {
  return (
    <div className="flex flex-col gap-4">
      <div className="flex select-none items-center gap-1 text-xs">
        <input
          type="checkbox"
          checked={descriptor.isMultiselect}
          onChange={() => {
            onChange({
              ...descriptor,
              isMultiselect: !descriptor.isMultiselect,
            });
          }}
        />
        Allow multiple
      </div>

      <div>
        <div className="grid-cols-m1111m grid w-full items-center gap-1">
          <div></div>
          <div className="select-none text-xs font-semibold">Label</div>
          <div className="select-none text-xs font-semibold">Back</div>
          <div className="select-none text-xs font-semibold">Text</div>
          <div className="select-none text-xs font-semibold">Border</div>
          <div></div>
          {descriptor.options.map((option, idx) => {
            return (
              <Fragment key={option.id}>
                <Icon icon="drag-handle-vertical" size={12} />
                <Input
                  className="w-full"
                  value={option.label}
                  onChange={(e) => {
                    const newOptions = [...descriptor.options];
                    newOptions[idx] = {
                      ...newOptions[idx],
                      label: e.target.value,
                    };

                    onChange({
                      ...descriptor,
                      options: newOptions,
                    });
                  }}
                />
                <Input
                  className="w-full"
                  value={option.bgColor}
                  onChange={(e) => {
                    const newOptions = [...descriptor.options];
                    newOptions[idx] = {
                      ...newOptions[idx],
                      bgColor: e.target.value,
                    };

                    onChange({
                      ...descriptor,
                      options: newOptions,
                    });
                  }}
                />
                <Input
                  className="w-full"
                  value={option.color}
                  onChange={(e) => {
                    const newOptions = [...descriptor.options];
                    newOptions[idx] = {
                      ...newOptions[idx],
                      color: e.target.value,
                    };

                    onChange({
                      ...descriptor,
                      options: newOptions,
                    });
                  }}
                />
                <Input
                  className="w-full"
                  value={option.border}
                  onChange={(e) => {
                    const newOptions = [...descriptor.options];
                    newOptions[idx] = {
                      ...newOptions[idx],
                      border: e.target.value,
                    };

                    onChange({
                      ...descriptor,
                      options: newOptions,
                    });
                  }}
                />
                <Icon
                  icon="small-cross"
                  size={12}
                  onClick={() => {
                    onChange({
                      ...descriptor,
                      options: descriptor.options.filter(({ id }) => id !== option.id),
                    });
                  }}
                />
              </Fragment>
            );
          })}
        </div>
        <div className="mt-1 flex justify-end">
          <Button
            onClick={() => {
              onChange({
                ...descriptor,
                options: [
                  ...descriptor.options,
                  {
                    id: `opt-${Math.floor(Math.random() * 10_000)}`,
                    label: `Option ${descriptor.options.length + 1}`,
                    color: 'rgb(87, 96, 106)',
                    bgColor: 'rgb(246, 248, 250)',
                    border: 'rgb(208, 215, 222)',
                  },
                ],
              });
            }}
          >
            <Icon icon="add-to-artifact" size={12} className="-scale-y-100" /> Add an option
          </Button>
        </div>
      </div>
    </div>
  );
};
