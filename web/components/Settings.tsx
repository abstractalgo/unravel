import { useState } from 'react';
import { FieldDesc } from 'unravel-common';
import { useWorkspace } from '../utils/useWorkspace';
import { FieldConfigurator } from './fields/configurators';
import cx from 'classnames';
import { Input } from './ui/Input';
import { FIELD_TYPE_ICON } from './fields/icons';
import { Button } from './ui/Button';
import { Icon } from '@blueprintjs/core';
import { Popover2 } from '@blueprintjs/popover2';

export const Settings = (): JSX.Element | null => {
  const { workspace, setWorkspace } = useWorkspace();

  const [descriptors, setDescriptors] = useState<FieldDesc[]>(workspace?.fieldDescriptors ?? []);

  const [expandedDescriptors, setExpandedDescriptors] = useState<Record<FieldDesc['id'], boolean>>({});

  const addFieldDescriptor = (desc: FieldDesc) => {
    setDescriptors([...descriptors, desc]);
  };

  const removeFieldDescriptor = (descId: FieldDesc['id']) => {
    setDescriptors(descriptors.filter((desc) => desc.id !== descId));
  };

  if (!workspace) {
    return null;
  }

  return (
    <div className="p-4">
      <h2 className="text-base font-semibold">{workspace.title}</h2>
      <p className="text-gray-500">{workspace.description}</p>

      <hr className="my-8" />

      <div className="mb-2">Fields:</div>

      {descriptors.length === 0 ? (
        <div className="text-xs italic text-gray-400">No custom fields.</div>
      ) : (
        <div className="border-border rounded border">
          {descriptors.map((desc, idx) => (
            <div
              key={desc.id}
              className={cx('p-2', {
                'border-border border-b': idx !== descriptors.length - 1,
              })}
              onClick={(e) => {
                if (expandedDescriptors[desc.id]) {
                  return;
                }

                setExpandedDescriptors({
                  ...expandedDescriptors,
                  [desc.id]: true,
                });
              }}
            >
              {expandedDescriptors[desc.id] ? (
                <div className="flex flex-col gap-4">
                  <div className="flex items-start justify-between">
                    <div>
                      {/* <div className="mb-1 select-none text-xs font-semibold">
                        Field name
                      </div> */}
                      <div className="flex items-center gap-2">
                        {FIELD_TYPE_ICON[desc.type]}
                        <Input
                          value={desc.name}
                          onChange={(e) => {
                            const newDescs = [...descriptors];
                            newDescs[idx] = {
                              ...newDescs[idx],
                              name: e.target.value,
                            };
                            setDescriptors(newDescs);
                          }}
                        />
                      </div>
                    </div>
                    <Button
                      onClick={() => {
                        removeFieldDescriptor(desc.id);
                      }}
                      title="Remove field"
                    >
                      <Icon icon="trash" size={12} />
                    </Button>
                  </div>

                  <FieldConfigurator
                    descriptor={desc}
                    onChange={(newDesc) => {
                      const newDescs = [...descriptors];
                      newDescs[idx] = newDesc;
                      setDescriptors(newDescs);
                    }}
                  />
                </div>
              ) : (
                <div className="flex cursor-pointer items-center gap-2 text-xs font-semibold">
                  <span>{FIELD_TYPE_ICON[desc.type]}</span>
                  {desc.name}
                </div>
              )}
            </div>
          ))}
        </div>
      )}

      <div className="mt-4 mb-8 flex items-center justify-end gap-1">
        {/* @ts-ignore */}
        <Popover2
          minimal
          usePortal={false}
          position="bottom-right"
          content={
            <div>
              <Button
                className="w-full rounded-none"
                onClick={() => {
                  const id = `desc-${Math.floor(Math.random() * 1000)}`;
                  addFieldDescriptor({
                    id,
                    type: 'select',
                    isMultiselect: false,
                    name: `Select field #${descriptors.length}`,
                    options: [
                      {
                        id: `${id}-opt-1`,
                        label: 'Option #1',
                        color: 'rgb(87, 96, 106)',
                        bgColor: 'rgb(246, 248, 250)',
                        border: 'rgb(208, 215, 222)',
                      },
                      {
                        id: `${id}-opt-2`,
                        label: 'Option #2',
                        color: 'rgb(87, 96, 106)',
                        bgColor: 'rgb(246, 248, 250)',
                        border: 'rgb(208, 215, 222)',
                      },
                    ],
                  });
                  setExpandedDescriptors({
                    ...expandedDescriptors,
                    [id]: true,
                  });
                }}
              >
                {FIELD_TYPE_ICON['select']} Select
              </Button>
              <Button
                className="w-full rounded-none"
                onClick={() => {
                  const id = `desc-${Math.floor(Math.random() * 1000)}`;
                  addFieldDescriptor({
                    id,
                    type: 'people',
                    name: `People ${descriptors.length}`,
                    isMultiselect: false,
                  });
                  setExpandedDescriptors({
                    ...expandedDescriptors,
                    [id]: true,
                  });
                }}
              >
                {FIELD_TYPE_ICON['people']} Collaborators
              </Button>
              <Button
                className="w-full rounded-none"
                onClick={() => {
                  const id = `desc-${Math.floor(Math.random() * 1000)}`;
                  addFieldDescriptor({
                    id,
                    type: 'text',
                    name: `Text field ${descriptors.length}`,
                  });
                  setExpandedDescriptors({
                    ...expandedDescriptors,
                    [id]: true,
                  });
                }}
              >
                {FIELD_TYPE_ICON['text']} Text
              </Button>
            </div>
          }
        >
          <Button>
            <Icon icon="add-row-bottom" size={12} /> Create new field
          </Button>
        </Popover2>
        <Button
          onClick={() => {
            setWorkspace({
              ...workspace,
              fieldDescriptors: descriptors,
            });
          }}
        >
          <Icon icon="confirm" size={12} /> Save
        </Button>
      </div>

      <div className="mb-4">Workflows...</div>

      <div className="mb-4">Collaborators...</div>
    </div>
  );
};
