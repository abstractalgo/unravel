import { KeyboardEventHandler, useEffect, useRef, useState } from 'react';

export const CommandPalette = (): JSX.Element => {
  const [text, setText] = useState('');
  const inputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    const kbdHandler: KeyboardEventHandler = (e) => {
      // Cmd/Ctrl + P
      if (e.key === 'P' && (e.ctrlKey || e.metaKey)) {
        e.preventDefault();
        e.stopPropagation();
        inputRef.current!.focus();
      }
    };

    // @ts-ignore
    document.body.addEventListener('keyup', kbdHandler);
    return () => {
      // @ts-ignore
      document.body.removeEventListener('keyup', kbdHandler);
    };
  }, []);

  return (
    <div className="text-xs">
      <div className="pointer-events-none absolute left-1 top-1 inline-block rounded border border-slate-300 bg-gray-50 px-1 py-0 text-slate-500">
        ⌘ + P
      </div>
      <input
        ref={inputRef}
        type="text"
        value={text}
        onChange={(e) => setText(e.target.value)}
        placeholder="Search..."
        className="rounded border border-gray-300 bg-white py-1 pl-[50px] shadow-lg"
      />
    </div>
  );
};
