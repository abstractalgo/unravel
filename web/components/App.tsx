import { useEffect, useState } from 'react';
import { mockData as mockData } from '../utils/mock_data';
import { useCollaboration } from '../utils/useCollaboration';
import { Link, useLinks } from '../utils/useLinks';
import { useSelection } from '../utils/useSelection';
import { Task, useTasks } from '../utils/useTasks';
import { useWorkspace } from '../utils/useWorkspace';
import { Graph } from './graph/Graph';
import { Nav } from './Nav';
import { Settings } from './Settings';
import { Sidebar } from './Sidebar';
import { Button } from './ui/Button';
import { Input } from './ui/Input';

export const App = (): JSX.Element => {
  const { setWorkspace } = useWorkspace();
  const { setCollaborators } = useCollaboration();
  const { setTasks, createTask, getTask } = useTasks();
  const { setLinks, createLink } = useLinks();
  const selection = useSelection();

  const [parentTaskId, setParentTaskId] = useState('task-');
  const [sourceTaskId, setSourceTaskId] = useState('task-');
  const [targetTaskId, setTargetTaskId] = useState('task-');
  const [linkType, setLinkType] = useState<Link['type']>('relates');

  const [mode, setMode] = useState<'app' | 'settings' | 'notifications' | 'history'>('app');

  useEffect(() => {
    setWorkspace(mockData.workspace);

    const tasks: Task[] = [];
    const links: Link[] = [];
    for (const task of mockData.tasks) {
      const { createdAt, createdBy, workspaceId, ...restTask } = task;
      tasks.push({
        ...restTask,
        links: [],
      });
    }

    for (const link of mockData.links) {
      const sourceTask = tasks.find((task) => task.id === link.source);
      const targetTask = tasks.find((task) => task.id === link.target);

      if (!sourceTask || !targetTask) {
        return;
      }

      const newLink: Link = {
        id: link.id,
        sourceTask,
        targetTask,
        type: link.type,
        ref: {
          current: null,
        },
      };

      sourceTask.links.push(newLink);
      targetTask.links.push(newLink);
      links.push(newLink);
    }

    setTasks(tasks);
    setLinks(links);
    setCollaborators(mockData.members);
  }, []);

  useEffect(() => {
    // getAllIssues();
  }, []);

  const createNewTask = () => {
    const parentTask = getTask(parentTaskId);
    if (!parentTask) {
      createTask({});
    } else {
      createTask({
        parent: parentTask,
      });
    }
  };

  const createNewLink = () => {
    const sourceTask = getTask(sourceTaskId);
    const targetTask = getTask(targetTaskId);
    if (!sourceTask || !targetTask) {
      return;
    }

    createLink({
      sourceTask,
      targetTask,
      type: 'relates',
    });
  };

  return (
    <main
      className="h-[100vh] w-[100vw]"
      onKeyDown={(e) => {
        if (e.key === 'Backspace') {
          e.stopPropagation();
        }
      }}
    >
      <Graph />
      <Nav onClick={setMode} />
      <div className="fixed bottom-0 left-0 border border-gray-300 bg-white shadow-md">
        <div className="flex items-center gap-2">
          <Input
            value={parentTaskId}
            onChange={(e) => setParentTaskId(e.target.value)}
            onKeyDown={(e) => {
              if (e.key === 'Enter') {
                createNewTask();
              }
            }}
          />
          <Button
            onClick={() => {
              createNewTask();
            }}
          >
            create task
          </Button>
        </div>
        <div className="flex items-center gap-2">
          <Input
            value={sourceTaskId}
            onChange={(e) => setSourceTaskId(e.target.value)}
            onKeyDown={(e) => {
              if (e.key === 'Enter') {
                createNewLink();
              }
            }}
          />
          <Input
            value={targetTaskId}
            onChange={(e) => setTargetTaskId(e.target.value)}
            onKeyDown={(e) => {
              if (e.key === 'Enter') {
                createNewLink();
              }
            }}
          />
          <select onChange={(e) => setLinkType(e.target.value as Link['type'])}>
            <option value="relates">relates</option>
            <option value="depends-on">depends-on</option>
            <option value="child">child</option>
          </select>
          <Button
            onClick={() => {
              createNewLink();
            }}
          >
            create link
          </Button>
        </div>
      </div>
      {(mode !== 'app' || selection.tasks.length > 0) && (
        <div
          className="fixed top-0 right-0 bottom-0
      w-[400px] border border-gray-300 bg-white shadow-md"
        >
          {mode === 'app' && selection.tasks.length > 0 && <Sidebar />}
          {mode === 'settings' && <Settings />}
        </div>
      )}
    </main>
  );
};
