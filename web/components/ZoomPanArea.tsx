import { MouseEventHandler, ReactNode, useRef, WheelEventHandler } from 'react';
import { clamp } from '../utils/math';
import { Position } from '../utils/types';

const ZOOM_FACTOR = -0.001;
const MIN_ZOOM = 0.1;
const MAX_ZOOM = 1.0;

export const transformations = {
  x: 0,
  y: 0,
  k: 1,
};

type Props = {
  children: ReactNode;
  onClick?: () => void;
  onChange?: (t: typeof transformations) => void;
};

export const ZoomPanArea = ({ children, onClick, onChange }: Props) => {
  const containerRef = useRef<HTMLDivElement>(null);

  const dragStartPosition = useRef<Position | null>(null);
  const hadMoveSinceDragStart = useRef(false);

  const onMouseDownHandler: MouseEventHandler<HTMLDivElement> = (e) => {
    // console.log("zoom pan - mouse down");

    dragStartPosition.current = {
      // @ts-ignore
      x: e.clientX,
      // @ts-ignore
      y: e.clientY,
    };
  };

  const onMouseMoveHandler: MouseEventHandler<HTMLDivElement> = (e) => {
    // console.log("zoom pan - move");

    if (dragStartPosition.current === null) {
      return;
    }

    if (!hadMoveSinceDragStart.current) {
      hadMoveSinceDragStart.current = true;
      document.body.style.userSelect = 'none';
    }

    transformations.x += e.movementX;
    transformations.y += e.movementY;

    containerRef.current!.style.transform = `translate(${transformations.x}px,${transformations.y}px) scale(${transformations.k})`;
    onChange?.(transformations);
  };

  const onMouseUpHandler: MouseEventHandler<HTMLDivElement> = (e) => {
    if (!hadMoveSinceDragStart.current) {
      document.body.style.userSelect = 'auto';
      onClick?.();
    }

    // reset
    hadMoveSinceDragStart.current = false;
    dragStartPosition.current = null;
  };

  const onWheelHandler: WheelEventHandler<HTMLDivElement> = (e) => {
    e.stopPropagation();
    // disallow zoom while dragging
    if (dragStartPosition.current !== null) {
      return;
    }

    // TODO zoom on mouse position as center

    const k = transformations.k + e.deltaY * ZOOM_FACTOR;
    transformations.k = clamp(k, MIN_ZOOM, MAX_ZOOM);
    containerRef.current!.style.transform = `translate(${transformations.x}px,${transformations.y}px) scale(${transformations.k})`;
    onChange?.(transformations);
  };

  return (
    <div
      onMouseDown={onMouseDownHandler}
      onMouseUp={onMouseUpHandler}
      onMouseMove={onMouseMoveHandler}
      onWheel={onWheelHandler}
      className="h-full max-h-full min-h-full w-full min-w-full max-w-full overflow-hidden"
    >
      <div ref={containerRef}>{children}</div>
    </div>
  );
};
