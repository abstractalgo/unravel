import { useEffect, useState } from 'react';
import { Comment, Event, EventType } from 'unravel-common';
import { Button } from './ui/Button';
import { CommentItem } from './Comment';
import { Popover2 } from '@blueprintjs/popover2';
import { Icon } from '@blueprintjs/core';
import { Task, useTasks } from '../utils/useTasks';
import { Link, useLinks } from '../utils/useLinks';
import { formatTimeDiff } from '../utils/date';

const EventRenderers: {
  [E in EventType]: (props: { event: Omit<Extract<Event, { type: E }>, 'type'> }) => JSX.Element;
} = {
  created: ({ event }) => {
    return (
      <div className="text-bp-gray7 flex items-center gap-1 text-[12px]">
        <Icon icon="calendar" size={12} />
        Created <span title={new Date(event.createdAt).toLocaleString()}>{formatTimeDiff(event.createdAt)}</span>
      </div>
    );
  },
};

type EventsSectionProps = {
  itemId: Task['id'] | Link['id'];
  itemType: 'task' | 'link';
};

export const EventsSection = ({ itemId, itemType }: EventsSectionProps): JSX.Element | null => {
  const { getTask } = useTasks();
  // const { getLink } = useLinks();

  const [sortOrder, setSortOrder] = useState<'new-first' | 'old-first'>('new-first');
  const [enabledCategories, setEnabledCategories] = useState({
    events: false,
    comments: true,
  });

  const [eventsAndComments, _setEventsAndComments] = useState<
    | (
        | {
            type: 'comment';
            comment: Comment;
          }
        | Event
      )[]
    | null
  >(null);

  const setEventsAndComments = (data: NonNullable<typeof eventsAndComments>): void => {
    _setEventsAndComments(
      data.sort((a, b) => {
        const aCreatedAt = a.type === 'comment' ? a.comment.createdAt : a.createdAt;
        const bCreatedAt = b.type === 'comment' ? b.comment.createdAt : b.createdAt;

        return aCreatedAt - bCreatedAt;
      }),
    );
  };

  useEffect(() => {
    // TODO fetch comments and sort them
    // TODO fetch events and sort them

    if (itemType === 'task') {
      const taks = getTask(itemId)!;
      setEventsAndComments([
        {
          type: 'comment',
          comment: {
            id: Math.floor(Math.random() * 1_000_000).toString(),
            authorId: 'user-1',
            createdAt: Date.now() - 15 * 60 * 1000,
            updatedAt: null,
            reactions: {},
            content: 'Hello world!',
          },
        },
        {
          type: 'comment',
          comment: {
            id: Math.floor(Math.random() * 1_000_000).toString(),
            authorId: 'user-2',
            createdAt: Date.now() - 14 * 60 * 1000,
            updatedAt: null,
            reactions: {},
            content: 'Hello 👋',
          },
        },
      ]);
    } else {
      // const link = getLink(itemId)!;
      setEventsAndComments([]);
    }
  }, [itemId, itemType]);

  const onCreateCommentHandler = () => {};
  const onUpdateCommentHandler = () => {};
  const onDeleteCommentHandler = () => {};

  if (!eventsAndComments) {
    return null;
  }

  return (
    <div className="events-section flex flex-col gap-3">
      <CommentItem comment={null} onCreate={onCreateCommentHandler} />

      {/* events: sort and filter */}
      {/* <div className="events-controls flex justify-end gap-1">
        <Popover2
          content={
            <div className="flex flex-col">
              <Button>All</Button>
              <Button>Only comments</Button>
              <Button>Only updates</Button>
            </div>
          }
          minimal
          position="bottom-right"
        >
          <Button className="text-gray-500">
            <Icon icon="filter-list" size={12} /> All
          </Button>
        </Popover2>
        <Popover2
          content={
            <div className="flex flex-col">
              <Button>Newest first</Button>
              <Button>Oldest first</Button>
            </div>
          }
          minimal
          position="bottom-right"
        >
          <Button className="text-gray-500">
            <Icon icon="sort" size={12} /> Newest first
          </Button>
        </Popover2>
      </div> */}

      <div className="events-list flex flex-col gap-3">
        {eventsAndComments.map((eventOrComment) => {
          if (eventOrComment.type === 'comment') {
            const comm = eventOrComment.comment;
            return (
              <CommentItem
                key={comm.id}
                comment={comm}
                onUpdate={onUpdateCommentHandler}
                onDelete={onDeleteCommentHandler}
              />
            );
          }

          const event = eventOrComment;
          const EventRenderer = EventRenderers[event.type];
          return <EventRenderer key={event.id} event={event} />;
        })}
      </div>
    </div>
  );
};
