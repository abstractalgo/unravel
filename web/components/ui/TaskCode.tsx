import { Workspace } from 'unravel-common';
import { Task } from '../../utils/useTasks';

type TaskCodeProps = {
  workspaceCode: Workspace['code'];
  code: Task['code'];
};

export const TaskCode = ({ workspaceCode, code }: TaskCodeProps): JSX.Element => {
  return (
    <div className="text-xxs pointer-events-none text-gray-500">
      {workspaceCode}-{code}
    </div>
  );
};
