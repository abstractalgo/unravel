import { ComponentProps } from 'react';
import cx from 'classnames';

export const Input = ({ className, style = {}, ...rest }: Omit<ComponentProps<'input'>, 'type'>): JSX.Element => (
  <input
    className={cx('rounded border py-1 px-0.5 text-xs text-gray-700 outline-0 focus:bg-white', className)}
    spellCheck={false}
    style={{
      maxHeight: '24px',
      ...style,
    }}
    {...rest}
  />
);
