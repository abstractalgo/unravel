import { Icon, IconName, IconProps } from '@blueprintjs/core';
import cx from 'classnames';
import { ComponentProps } from 'react';

type ButtonProps = ComponentProps<'button'> & {
  icon?: IconName;
  iconProps?: Omit<IconProps, 'size'>;
};

export const Button = ({ className, children, ...props }: ButtonProps): JSX.Element => {
  return (
    <button
      className={cx(
        'text-primary hover:bg-bp-gray3 flex items-center gap-1 rounded p-1.5 text-xs leading-none',
        className,
      )}
      {...props}
    >
      {children}
    </button>
  );
};
