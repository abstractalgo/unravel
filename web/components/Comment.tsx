import { Icon } from '@blueprintjs/core';
import { Popover2 } from '@blueprintjs/popover2';
import { useState } from 'react';
import { type Comment } from 'unravel-common';
import { formatTimeDiff } from '../utils/date';
import { useCollaboration } from '../utils/useCollaboration';
import { Button } from './ui/Button';

type CommentProps =
  | {
      comment: Comment;
      onUpdate: () => void;
      onDelete: () => void;
    }
  | {
      comment: null;
      onCreate: () => void;
    };

export const CommentItem = ({ comment }: CommentProps): JSX.Element => {
  const { collaborators } = useCollaboration();
  const [editable, setEditable] = useState(false);
  const [content, setContent] = useState(comment ? comment.content : '');

  const author = collaborators.find((c) => c.id === comment?.authorId);

  if (comment && !editable) {
    return (
      <div className="comment-section">
        <div className="flex items-center justify-between">
          <div className="text-bp-gray7 flex items-center gap-1 text-[12px]">
            <Icon icon="chat" size={12} /> {author?.displayName} commented {formatTimeDiff(comment.createdAt)}
          </div>
          <div className="comment-controls">
            {/* @ts-ignore */}
            <Popover2
              minimal
              position="bottom-right"
              content={
                <div className="text-bp-gray7 flex flex-col items-stretch">
                  <Button className="rounded-none">
                    <Icon icon="edit" size={12} /> Edit
                  </Button>
                  <Button className="rounded-none">
                    <Icon icon="trash" size={12} /> Delete
                  </Button>
                  <hr />
                  <Button className="rounded-none">
                    <Icon icon="link" size={12} /> Copy URL
                  </Button>
                </div>
              }
            >
              <Button>
                <Icon icon="more" className="rotate-90" size={12} />
              </Button>
            </Popover2>
          </div>
        </div>
        <div className="ml-4">
          <div className="text-bp-gray12 text-sm">{comment.content}</div>
          {/* <div className="text-bp-gray7">
            <Icon icon="emoji" size={12} />
          </div> */}
        </div>
      </div>
    );
  }

  return (
    <div className="rounded border border-gray-200 p-1">
      <textarea
        className="w-full resize-none"
        rows={3}
        value={content}
        onChange={(e) => {
          setContent(e.target.value);
        }}
      />
      <div className="flex items-center justify-between">
        <Button title="Add files" className="text-bp-gray12">
          <Icon icon="paperclip" size={12} />
        </Button>
        <Button title="Add files" className="text-bp-gray12">
          Comment
        </Button>
      </div>
    </div>
  );
};
