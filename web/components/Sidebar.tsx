import { Icon } from '@blueprintjs/core';
import { Popover2 } from '@blueprintjs/popover2';
import cx from 'classnames';
import { Fragment, useMemo } from 'react';
import { useSelection } from '../utils/useSelection';
import { Task, useTasks } from '../utils/useTasks';
import { useWorkspace } from '../utils/useWorkspace';
import { Button } from './ui/Button';
import { EventsSection } from './EventsSection';
import { FieldRenderer } from './fields/renderers';
import { Input } from './ui/Input';
import { TaskCode } from './ui/TaskCode';

export const Sidebar = (): JSX.Element | null => {
  const selection = useSelection();
  const { workspace } = useWorkspace();
  const { getTask, updateTaskTitle } = useTasks();

  const tasks = useMemo(() => {
    return selection.tasks
      .map((taskId) => {
        return getTask(taskId);
      })
      .filter((t) => !!t) as Task[];
  }, [getTask, selection.tasks]);

  if (!workspace) {
    return null;
  }

  return (
    <div className="text-primary">
      <div className="flex items-center justify-between gap-1 px-3 pt-3">
        <div className="flex items-center gap-1">
          <Button title="Copy issue URL">
            <Icon icon="link" size={12} />
          </Button>
          {/* <Button title="Copy issue ID">
            <Icon icon="numerical" size={12} />
          </Button> */}
        </div>
        <div className="flex items-center gap-1">
          <Button>
            <Icon icon="new-link" size={12} /> Link
          </Button>
          <Button>
            <Icon icon="backlink" size={12} /> New ref
          </Button>
          <Button>
            <Icon icon="duplicate" size={12} /> Duplicate
          </Button>
          {/* <Button title="Prev issue">
            <Icon icon="chevron-up" size={12} />
          </Button>
          <Button title="Next issue">
            <Icon icon="chevron-down" size={12} />
          </Button> */}
          {/* @ts-ignore */}
          <Popover2
            usePortal={false}
            minimal
            position="bottom-right"
            content={
              <div className="flex flex-col">
                <Button className="rounded-none">
                  <Icon icon="notifications" size={12} /> Subscribe
                </Button>
                <Button className="rounded-none">
                  <Icon icon="feed" size={12} /> Connect Slack
                </Button>
                <hr />
                <Button className="rounded-none">
                  <Icon icon="lock" size={12} /> Lock
                </Button>
                <Button className="rounded-none">
                  <Icon icon="archive" size={12} /> Archive
                </Button>
                <hr />
                <Button className="rounded-none">
                  <Icon icon="trash" size={12} /> Delete
                </Button>
              </div>
            }
          >
            <Button>
              <Icon icon="more" size={12} className="rotate-90" />
            </Button>
          </Popover2>
        </div>
      </div>

      {tasks.map((task) => (
        <div key={task.id} className="p-4">
          {/* task header */}
          <TaskCode workspaceCode={workspace.code} code={task.code} />
          <input
            type="text"
            className="focus:border-border w-full rounded border border-transparent text-sm font-semibold"
            value={task.title}
            spellCheck={false}
            onChange={(e) => {
              updateTaskTitle(task, e.target.value);
            }}
          />

          {/* task fields */}
          <div className="grid-cols-m1 my-4 grid gap-x-4 gap-y-1 text-xs text-gray-500">
            {workspace.fieldDescriptors.map((desc) => {
              return (
                <Fragment key={desc.id}>
                  <div className="leading-6">{desc.name}:</div>
                  <div className="flex items-center">
                    <FieldRenderer task={task} fieldDescriptorId={desc.id} mode="full" />
                  </div>
                </Fragment>
              );
            })}
          </div>

          {/* events & comments */}
          <EventsSection itemId={task.id} itemType="task" />
        </div>
      ))}
    </div>
  );
};
