import { Icon } from '@blueprintjs/core';
import { Button } from './ui/Button';
import { CommandPalette } from './CommandPalette';
import cx from 'classnames';
import { useWorkspace } from '../utils/useWorkspace';

type NavProps = {
  onClick: (mode: 'settings' | 'history' | 'notifications') => void;
};

export const Nav = ({ onClick }: NavProps): JSX.Element | null => {
  const { workspace } = useWorkspace();

  if (!workspace) {
    return null;
  }

  return (
    <div className="pointer-events-none fixed top-2 left-2 right-2 flex items-center justify-between">
      <div className="items-top pointer-events-none flex gap-2">
        <CommandPalette />
        <div>{}</div>
      </div>

      <div className="flex items-center gap-4">
        <div className="flex items-center gap-2">
          <div className="flex items-center gap-1">
            {[1, 2].map((user) => (
              <div
                key={user}
                className={cx('h-5 w-5 rounded-full', {
                  'bg-green-700': user === 1,
                  'bg-pink-600': user === 2,
                })}
              ></div>
            ))}
          </div>
          <Button className="border border-gray-400">Share</Button>
        </div>
        <div className="flex items-center gap-2 text-gray-400">
          <Icon icon="notifications" size={13} />
          <Icon icon="history" size={13} />
          {/* <Icon icon="help" size={13} /> */}
          <Icon icon="cog" size={13} onClick={() => onClick('settings')} />
          {/* 
            workspace settings
            workflows / automation
            reports / plugins
            templates
            help
            user settings ↗
          */}
        </div>
      </div>
    </div>
  );
};
