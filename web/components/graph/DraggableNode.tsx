import { MouseEventHandler, ReactNode, useRef } from 'react';
import { Position } from '../../utils/types';
import { transformations } from '../ZoomPanArea';

type Props = {
  children: ReactNode;
  position: Position;
  onMouseDown?: () => void;
  onDragStart?: () => void;
  onDrag?: (diff: Position) => void;
  onDragEnd?: (diff: Position) => void;
  onClick?: () => void;
};

export const DraggableNode = ({
  children,
  position,
  onMouseDown,
  onDragStart,
  onDrag,
  onDragEnd,
  onClick,
}: Props): JSX.Element => {
  const dragStartPosition = useRef<null | Position>(null);
  const hadMoveSinceDragStart = useRef(false);
  const ref = useRef<HTMLDivElement>(null);

  const onMouseDownHandler: MouseEventHandler<HTMLDivElement> = (e) => {
    if (e.button !== 0) {
      return;
    }

    e.stopPropagation();

    // console.log("mouse down");

    dragStartPosition.current = {
      x: e.clientX,
      y: e.clientY,
    };

    // @ts-ignore
    document.body.addEventListener('mousemove', onMouseMoveHandler);
    document.body.style.userSelect = 'none';

    // action
    // onDragStart();
    onMouseDown?.();
  };

  const onMouseMoveHandler: MouseEventHandler = (e) => {
    if (dragStartPosition.current === null) {
      return;
    }

    if (!hadMoveSinceDragStart.current) {
      hadMoveSinceDragStart.current = true;
      ref.current!.style.cursor = 'grab';
      onDragStart?.();
    }

    const diffX = (e.clientX - dragStartPosition.current.x) / transformations.k;
    const diffY = (e.clientY - dragStartPosition.current.y) / transformations.k;

    const x = position.x + diffX;
    const y = position.y + diffY;

    ref.current!.style.transform = `translate(${x}px, ${y}px)`;

    // trigger action
    onDrag?.({
      x: diffX,
      y: diffY,
    });
  };

  const onMouseUpHandler: MouseEventHandler<HTMLDivElement> = (e) => {
    e.stopPropagation();
    if (e.buttons % 2 !== 0 || !dragStartPosition.current) {
      return;
    }

    // console.log("mouse up");

    // trigger actions
    if (hadMoveSinceDragStart.current) {
      const diffX = (e.clientX - dragStartPosition.current.x) / transformations.k;
      const diffY = (e.clientY - dragStartPosition.current.y) / transformations.k;
      onDragEnd?.({
        x: diffX,
        y: diffY,
      });
    } else {
      onClick?.();
    }

    // reset
    hadMoveSinceDragStart.current = false;
    dragStartPosition.current = null;
    // @ts-ignore
    document.removeEventListener('mousemove', onMouseMoveHandler);
    document.body.style.userSelect = 'auto';
    ref.current!.style.cursor = 'pointer';
  };

  return (
    <div
      ref={ref}
      className="absolute inline-block cursor-pointer"
      style={{
        transform: `translate(${position.x}px, ${position.y}px)`,
      }}
      onMouseDown={onMouseDownHandler}
      onMouseUp={onMouseUpHandler}
    >
      {children}
    </div>
  );
};
