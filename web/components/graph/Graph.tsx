import { useEffect, useMemo, useRef, useState } from 'react';
import { Link, useLinks } from '../../utils/useLinks';
import { useSelection } from '../../utils/useSelection';
import { isTaskProject, Task, useTasks } from '../../utils/useTasks';
import { DraggableNode } from './DraggableNode';
import { GraphLink, genPath } from './Link';
import { GraphNode } from './Node';
import { ZoomPanArea } from '../ZoomPanArea';

export const Graph = (): JSX.Element => {
  const { tasks, getTask, updateTaskPositions } = useTasks();
  const { links } = useLinks();
  const selection = useSelection();

  const [size, setSize] = useState({
    width: 0,
    height: 0,
  });

  const canvasRef = useRef<HTMLDivElement>(null);
  const svgRef = useRef<SVGSVGElement>(null);
  const linksContainerRef = useRef<SVGGElement>(null);

  const projects = useMemo(() => tasks.filter(isTaskProject), [tasks]);

  useEffect(() => {
    const width = canvasRef.current!.offsetWidth;
    const height = canvasRef.current!.offsetHeight;
    setSize({
      width,
      height,
    });
  }, []);

  return (
    <div
      id="canvas"
      ref={canvasRef}
      className="fixed h-full max-h-full min-h-full w-full min-w-full max-w-full overflow-hidden"
      onClick={(e) => {
        // @ts-ignore
        if (e.target.id === 'canvas') {
          selection.selectTasks();
        }
      }}
    >
      {/* links */}
      <svg
        ref={svgRef}
        className="pointer-events-none fixed top-0 left-0 bottom-0 right-0"
        style={{
          width: `${size.width}`,
          height: `${size.height}`,
        }}
        viewBox={`0 0 ${size.width} ${size.height}`}
      >
        <g ref={linksContainerRef}>
          {links
            .filter((link) => link.type !== 'child')
            .map((link) => (
              <GraphLink key={link.id} link={link} ref={link.ref} />
            ))}
        </g>
      </svg>
      <ZoomPanArea
        onClick={() => selection.selectTasks()}
        onChange={(t) => {
          linksContainerRef.current!.setAttribute('transform', `translate(${t.x},${t.y}) scale(${t.k})`);
        }}
      >
        {/* projects & tasks */}
        {projects.map((project) => (
          <DraggableNode
            key={project.id}
            position={{ x: project.x, y: project.y }}
            onClick={() => {
              selection.selectTasks([project.id]);
            }}
            onMouseDown={() => {
              selection.selectTasks([project.id]);
            }}
            onDrag={(diff) => {
              const tasks = selection.tasks.map(getTask) as Task[];

              for (const task of tasks) {
                task.x += diff.x;
                task.y += diff.y;
              }

              const updatedLinks: Record<Link['id'], boolean> = {};
              for (const project of tasks) {
                for (const link of project.links) {
                  if (link.type === 'child') {
                    for (const _link of link.targetTask.links) {
                      if (!updatedLinks[_link.id]) {
                        _link.ref.current?.setAttribute('d', genPath(_link));
                        updatedLinks[_link.id] = true;
                      }
                    }
                  } else {
                    if (!updatedLinks[link.id]) {
                      link.ref.current?.setAttribute('d', genPath(link));
                      updatedLinks[link.id] = true;
                    }
                  }
                }
              }

              for (const task of tasks) {
                task.x -= diff.x;
                task.y -= diff.y;
              }
            }}
            onDragEnd={(diff) => {
              updateTaskPositions([project], diff);
            }}
          >
            <GraphNode project={project} groupBy={null} query="" />
          </DraggableNode>
        ))}
      </ZoomPanArea>
    </div>
  );
};
