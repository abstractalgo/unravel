import React, { ReactNode, ComponentProps, useRef, useEffect } from 'react';
import { FieldDesc } from 'unravel-common';
import { FieldRenderer } from '../fields/renderers';
import cx from 'classnames';
import { useSelection } from '../../utils/useSelection';
import { Task, useTasks } from '../../utils/useTasks';
import { useWorkspace } from '../../utils/useWorkspace';
import { Icon } from '@blueprintjs/core';
import { Popover2 } from '@blueprintjs/popover2';
import { Button } from '../ui/Button';
import { TaskCode } from '../ui/TaskCode';

const Cell = ({
  children,
  className,
  ...props
}: ComponentProps<'div'> & {
  children: ReactNode;
}): JSX.Element => {
  return (
    <div className={cx('border-border flex h-full w-full items-center border-t border-r', className)} {...props}>
      {children}
    </div>
  );
};

type GraphNodeProps = {
  project: Task;
  // layout
  layout?: {
    showId?: boolean;
    fields: {
      fieldDescriptorId: FieldDesc['id'];
      visible?: boolean;
      width?: number | 'auto';
    }[];
    width: number | 'auto';
  };
  groupBy: FieldDesc['id'] | null;
  // sort
  sort?: 'asc' | 'desc';
  // filter
  query: string;
  // filter by prop values (or github-like in the query)
};

export const GraphNode = ({
  project,
  groupBy,
  sort = 'asc',
  query,
  layout: _layout,
}: GraphNodeProps): JSX.Element | null => {
  const selection = useSelection();
  const { createTask, updateTaskTitle } = useTasks();
  const { workspace } = useWorkspace();

  const containerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (!containerRef.current) {
      return;
    }

    const container = containerRef.current;

    const observer = new ResizeObserver(([container]) => {
      project.width = container.contentRect.width;
    });
    observer.observe(container);

    return () => {
      observer.unobserve(container);
    };
  });

  if (!workspace) {
    return null;
  }

  const tasks = project.links
    .filter((link) => link.type === 'child' && link.sourceTask.id === project.id)
    .map((link) => link.targetTask);

  const isSelected = selection.isTaskSelected(project.id);

  const layout = _layout ?? {
    showId: false,
    fields: workspace.fieldDescriptors.map((desc) => ({
      fieldDescriptorId: desc.id,
      visible: true,
      width: 'auto',
    })),
    width: 'auto',
  };
  return (
    <div
      id={`project-${project.id}`}
      ref={containerRef}
      onClick={(e) => {
        // @ts-ignore
        if (e.target.id === `project-${project.id}`) {
          selection.selectTasks([project.id]);
        }
      }}
      style={{
        width: layout.width === 'auto' ? 'auto' : `${layout.width}px`,
      }}
      className={cx(
        'project border-border text-primary over relative inline-block rounded-[6px] border bg-white text-sm',
        {
          'shadow-card hover:shadow-card-hover': !isSelected,
          'border-focus shadow-card-focus': isSelected,
        },
      )}
    >
      {/* project's header */}
      <div className="p-2">
        <div className="flex items-start justify-between gap-6">
          <div>
            <TaskCode workspaceCode={workspace.code} code={project.code} />
            <div className="flex items-center gap-2">
              <div className="text-sm font-semibold">{project.title}</div>
              {workspace.fieldDescriptors.map((desc) => {
                const value = project.fields.find((f) => f.fieldDescriptorId === desc.id)?.value || [];

                if (
                  value.filter((v) => !!v).length === 0
                  // || !visibleFields.includes(desc.id)
                ) {
                  return null;
                }

                return (
                  <div key={desc.id} className="text-primary flex items-center text-xs">
                    <FieldRenderer task={project} fieldDescriptorId={desc.id} mode="simple-no-bd" />
                  </div>
                );
              })}
            </div>
          </div>

          {/* actions */}
          <div className="controls text-primary absolute left-full top-0 ml-2 flex flex-col items-center gap-1 opacity-0">
            {/* add task */}
            <Button
              onClick={() => {
                createTask({
                  parent: project,
                });
              }}
            >
              <Icon icon="plus" size={12} />
            </Button>
            {/* grid filters */}
            {tasks.length > 0 && (
              // @ts-ignore
              <Popover2
                content={
                  <div className="bg-white text-xs text-gray-400">
                    <div>layout</div>
                    <div>props</div>
                    <div>filter</div>
                    <div>sort</div>
                    <div>search</div>
                  </div>
                }
                interactionKind="click"
                minimal
                position="right-top"
                usePortal={false}
              >
                <Button>
                  <Icon icon="th-filtered" size={12} />
                </Button>
              </Popover2>
            )}
          </div>
        </div>
      </div>

      {/* table */}
      {tasks.length > 0 && (
        <div className="relative" onMouseDown={(e) => e.stopPropagation()}>
          {/* headers */}
          <div
            className="grid grid-flow-row items-center"
            style={{
              gridTemplateColumns: [
                ...Array.from({ length: layout.fields.length + (layout.showId === true ? 2 : 1) }).map(
                  () => 'max-content',
                ),
                'auto',
              ].join(' '),
            }}
          >
            <Cell className="bg-gray-50 px-1 py-0.5 text-xs text-gray-500">#</Cell>
            {layout.showId && <Cell className="bg-gray-50 px-1 py-0.5 text-xs text-gray-500">ID</Cell>}
            <Cell className="bg-gray-50 px-1 py-0.5 text-xs text-gray-500">Title</Cell>
            {workspace.fieldDescriptors.map(({ id, name }, idx) => (
              <Cell
                key={id}
                className={cx('bg-gray-50 px-1 py-0.5 text-xs text-gray-500', {
                  'border-r-0': idx === workspace.fieldDescriptors.length - 1,
                })}
              >
                {name}
              </Cell>
            ))}

            {/* tasks */}
            {tasks.map((task, idx) => {
              if (!task) {
                return null;
              }

              const isSelected = selection.isTaskSelected(task.id);

              return (
                /* task row */
                <>
                  {/* <div
                key={task.id}
                className={cx('relative grid w-full grid-flow-row items-center')}
                style={{
                  height: '25px',
                  gridTemplateColumns: `repeat(${workspace.fieldDescriptors.length + 2}, 100px)`,
                }}
                
              > */}
                  {/* selection border */}
                  <div
                    onClick={(e) => {
                      e.stopPropagation();
                      selection.selectTasks([task.id]);
                    }}
                    className={cx('absolute rounded border', {
                      'border-transparent': !isSelected,
                      'border-focus shadow-card-focus': isSelected,
                    })}
                    style={{
                      left: '-1px',
                      top: `${21 + idx * 25}px`,
                      height: '26px',
                      right: '-1px',
                    }}
                  ></div>
                  {/* task cells */}
                  <Cell className="text-xxs pointer-events-none h-[25px] px-1 py-0.5 text-gray-500">{idx + 1}</Cell>
                  {layout.showId && (
                    <Cell className="h-[25px] px-1 py-0.5">
                      <TaskCode workspaceCode={workspace.code} code={task.code} />
                    </Cell>
                  )}
                  <Cell className="text-primary relative h-[25px] max-w-[200px] overflow-hidden px-1 py-0.5 text-sm">
                    <span className="text-transparent">{task.title}</span>
                    <input
                      className="absolute left-1 right-1 top-0.5 bottom-0.5 overflow-hidden"
                      type="text"
                      value={task.title}
                      spellCheck={false}
                      onChange={(e) => {
                        updateTaskTitle(task, e.target.value);
                      }}
                    />
                  </Cell>
                  {workspace.fieldDescriptors.map((desc, idx) => {
                    return (
                      <Cell
                        key={desc.id}
                        className={cx('text-primary flex h-[25px] items-center text-xs', {
                          'border-r-0': idx === workspace.fieldDescriptors.length - 1,
                          'px-1 py-0.5': desc.type === 'text',
                        })}
                      >
                        <FieldRenderer task={task} fieldDescriptorId={desc.id} />
                      </Cell>
                    );
                  })}
                </>
              );
            })}
          </div>
        </div>
      )}
    </div>
  );
};
