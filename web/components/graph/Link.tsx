import { forwardRef, RefObject, useRef } from 'react';
import { Position } from '../../utils/types';
import { Link } from '../../utils/useLinks';
import { getTaskParent, isTaskProject, Task } from '../../utils/useTasks';

const PROJECT_LINK_OFFSET_Y = 20;
const TASK_LINK_OFFSET_Y = 85;
const TASK_LINK_OFFSET_SIZE = 25;

type LinkRef = SVGPathElement;

type LinkProps = {
  link: Link;
  selected?: boolean;
};

const calcEndpoint = (task: Task, type: 'start' | 'end'): Position => {
  // if it's a project, the endpoint's const near the top
  if (isTaskProject(task)) {
    return {
      x: task.x + (type === 'start' ? task.width ?? 0 : 0),
      y: task.y + PROJECT_LINK_OFFSET_Y,
    };
  }

  // ...otherwise, calculate how far along the vertical edge it is from the top
  const parentTask = getTaskParent(task)!;
  const idx = parentTask.links.filter((link) => link.type === 'child').findIndex((link) => link.targetTask === task);
  return {
    x: parentTask.x + (type === 'start' ? parentTask.width ?? 0 : 0),
    y: parentTask.y + TASK_LINK_OFFSET_Y + idx * TASK_LINK_OFFSET_SIZE,
  };
};

export const genPath = (link: Link): string => {
  const startPos: Position = calcEndpoint(link.sourceTask, 'start');
  const endPos: Position = calcEndpoint(link.targetTask, 'end');

  return `
    M ${startPos.x},${startPos.y}
    C ${startPos.x + 60},${startPos.y} ${endPos.x - 60},${endPos.y} ${endPos.x},${endPos.y}
  `;
};

export const GraphLink = forwardRef<LinkRef, LinkProps>(({ link, selected }: LinkProps, ref): JSX.Element | null => {
  const path = genPath(link);

  return (
    <g>
      <path ref={ref} id={`link-${link.id}`} d={path} fill="none" strokeWidth={1} stroke="#aaa" />
    </g>
  );
});

GraphLink.displayName = 'GraphLink';
