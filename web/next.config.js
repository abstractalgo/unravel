/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
};

// see: https://github.com/martpie/next-transpile-modules
const withTM = require('next-transpile-modules')(['unravel-common']);

module.exports = withTM(nextConfig);
