import type { NextPage } from 'next';
import Head from 'next/head';
import { App } from '../components/App';
import { CollaborationProvider } from '../utils/useCollaboration';
import { LinksProvider } from '../utils/useLinks';
import { SelectionProvider } from '../utils/useSelection';
import { TasksProvider } from '../utils/useTasks';
import { WorkspaceProvider } from '../utils/useWorkspace';

const Home: NextPage = () => {
  return (
    <div>
      <Head>
        <title>unravel</title>
        <meta name="description" content="unravel" />
        <link
          rel="icon"
          href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>🧶</text></svg>"
        />
      </Head>

      <WorkspaceProvider>
        <CollaborationProvider>
          <LinksProvider>
            <TasksProvider>
              <SelectionProvider>
                <App />
              </SelectionProvider>
            </TasksProvider>
          </LinksProvider>
        </CollaborationProvider>
      </WorkspaceProvider>
    </div>
  );
};

export default Home;
