import { useEffect, useRef } from 'react';

const CanvasPage = (): JSX.Element => {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const containerRef = useRef<HTMLDivElement>(null);
  const ctxRef = useRef<CanvasRenderingContext2D>();

  useEffect(() => {
    const w = containerRef.current?.clientWidth ?? 0;
    const h = containerRef.current?.clientHeight ?? 0;

    canvasRef.current!.width = w * window.devicePixelRatio;
    canvasRef.current!.style.width = `${w}px`;

    canvasRef.current!.height = h * window.devicePixelRatio;
    canvasRef.current!.style.height = `${h}px`;

    ctxRef.current = canvasRef.current!.getContext('2d')!;

    const ctx = ctxRef.current!;

    ctx.beginPath();
    ctx.moveTo(100, 100);
    ctx.lineTo(200, 100);
    ctx.strokeStyle = 'blue';
    ctx.lineWidth = 1;
    // ctx.closePath();
    ctx.stroke();
    // ctx.clearRect(0, 0, w, h);
  }, []);

  return (
    <div className="fixed h-[100vh] w-[100vw]" ref={containerRef}>
      <canvas ref={canvasRef}></canvas>
    </div>
  );
};

export default CanvasPage;
