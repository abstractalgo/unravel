import { Stage } from '@inlet/react-pixi';
import { useEffect, useRef } from 'react';

const PixiPage = (): JSX.Element => {
  const containerRef = useRef<HTMLDivElement>(null);
  // const stageRef = useRef<Stage>(null);

  useEffect(() => {
    const w = containerRef.current?.clientWidth ?? 0;
    const h = containerRef.current?.clientHeight ?? 0;

    // stageRef.current?.
  }, []);

  return (
    <div className="fixed h-[100vh] w-[100vw]" ref={containerRef}>
      <Stage width={500} height={500} />
    </div>
  );
};

export default PixiPage;
