import type { NextPage } from 'next';
import Head from 'next/head';

const Home: NextPage = () => {
  return (
    <div>
      <Head>
        <title>Unravel</title>
        <meta name="description" content="Unravel" />
        <link
          rel="icon"
          href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>🧶</text></svg>"
        />
      </Head>

      <main className="p-8">
        <h1 className="text-lg font-semibold">🧶 Unravel</h1>
        <p>graph-based project management</p>

        <div className="[&>a]:text-sky-600 my-6 flex items-center gap-1">
          follow:
          <a href="https://twitter.com/unravel_dev">@unravel_dev</a>
        </div>

        <form name="newsletter" action="/" method="POST" data-netlify="true">
          <input type="hidden" name="form-name" value="newsletter" />
          <div className="flex items-center">
            <input
              type="text"
              name="email"
              id="email"
              placeholder="your email"
              className="border border-slate-200 px-1"
            />
            <button
              type="submit"
              className="inline-block border border-slate-200 bg-slate-200 px-1 hover:bg-slate-300 active:bg-slate-400"
            >
              subscribe
            </button>
          </div>
        </form>
      </main>
    </div>
  );
};

export default Home;
