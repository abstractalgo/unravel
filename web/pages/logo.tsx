const LogoPage = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 100 100"
      style={{
        width: 256,
        height: 256,
      }}
    >
      <text y=".9em" fontSize="90">
        🧶
      </text>
    </svg>
  );
};

export default LogoPage;
