import { createContext, ReactNode, useContext, useEffect, useRef, useState } from 'react';
import { FieldDesc, TaskDTO } from 'unravel-common';
import { Position } from './types';
import { Link, useLinks } from './useLinks';
import { useWorkspace } from './useWorkspace';

export type Task = Pick<TaskDTO, 'id' | 'code' | 'fields' | 'height' | 'width' | 'x' | 'y' | 'title'> & {
  links: Link[];
};

type TasksCtx = {
  tasks: Task[];
  getTask: (taskId: Task['id']) => Task | null;
  updateTaskField: (options: { task: Task; fieldDescriptorId: FieldDesc['id']; value: string[] }) => void;
  updateTaskTitle: (task: Task, title: string) => void;
  updateTaskPositions: (tasks: Task[], diff: Position) => void;
  createTask: (options: {
    position?: Position;
    width?: number;
    height?: number;
    title?: string;
    parent?: Task;
  }) => Task;
  setTasks: (tasks: Task[]) => void;
};

const TasksContext = createContext<TasksCtx>({
  tasks: [],
  getTask: () => null,
  // @ts-ignore
  createTask: () => null,
  updateTaskTitle: () => {},
  updateTaskField: () => {},
  updateTaskPositions: () => {},
  setTasks: () => {},
});

export const TasksProvider = ({ children }: { children: ReactNode }): JSX.Element => {
  const { createLink } = useLinks();
  const [tasks, setTasks] = useState<Task[]>([]);
  const tasksRef = useRef<Record<Task['id'], Task>>({});

  useEffect(() => {
    tasksRef.current = {};
    for (const task of tasks) {
      tasksRef.current[task.id] = task;
    }
  }, [tasks]);

  const getTask: TasksCtx['getTask'] = (taskId) => {
    return tasks.find((t) => t.id === taskId) ?? null;
  };

  const createTask: TasksCtx['createTask'] = ({ parent, ...options }) => {
    const newTask: Task = {
      id: `task-${tasks.length + 1}`,
      code: tasks.length + 1,
      fields: [],
      width: options.width ?? 200,
      height: options.height ?? 100,
      x: options.position?.x ?? 0,
      y: options.position?.x ?? 0,
      title: options.title ?? `Task ${tasks.length + 1}`,
      links: [],
    };

    if (parent) {
      createLink({
        type: 'child',
        sourceTask: parent,
        targetTask: newTask,
      });
    }

    setTasks([...tasks, newTask]);

    return newTask;
  };

  const updateTaskField: TasksCtx['updateTaskField'] = ({ task, fieldDescriptorId, value }) => {
    const field = task.fields.find((field) => field.fieldDescriptorId === fieldDescriptorId);
    if (!field) {
      task.fields = [
        ...task.fields,
        {
          fieldDescriptorId,
          value,
        },
      ];
    } else {
      field.value = value;
    }

    setTasks([...tasks]);
  };

  const updateTaskTitle: TasksCtx['updateTaskTitle'] = (task, title) => {
    task.title = title;
    setTasks([...tasks]);
  };

  const updateTaskPositions: TasksCtx['updateTaskPositions'] = (updateTasks, diff) => {
    for (const task of updateTasks) {
      task.x += diff.x;
      task.y += diff.y;
    }

    setTasks([...tasks]);
  };

  return (
    <TasksContext.Provider
      value={{
        tasks,
        getTask,
        createTask,
        updateTaskField,
        updateTaskTitle,
        updateTaskPositions,
        setTasks,
      }}
    >
      {children}
    </TasksContext.Provider>
  );
};

export const useTasks = () => useContext(TasksContext);

export const isTaskProject = (task: Task): boolean =>
  task.links.every((link) => link.type !== 'child' || link.targetTask !== task);

export const getTaskParent = (task: Task): Task | null => {
  const link = task.links.find((link) => link.type === 'child' && link.targetTask === task);
  if (!link) {
    return null;
  }
  return link.sourceTask;
};
