import { createContext, ReactNode, useContext, useEffect, useState } from 'react';
import { Link, useLinks } from './useLinks';
import { Task, useTasks } from './useTasks';

type SelectionCtx = {
  tasks: Task['id'][];
  links: Link['id'][];
  selectTasks: (taskIds?: Task['id'][]) => void;
  deselectTasks: (taskId: Task['id'][]) => void;
  isTaskSelected: (taskId: Task['id']) => boolean;
  selectLinks: (linkIds?: Link['id'][]) => void;
  deselectLinks: (linkIds: Link['id'][]) => void;
};

const SelectionContext = createContext<SelectionCtx>({
  tasks: [],
  links: [],
  selectTasks: () => {},
  deselectTasks: () => {},
  isTaskSelected: () => false,
  selectLinks: () => {},
  deselectLinks: () => {},
});

export const SelectionProvider = ({ children }: { children: ReactNode }): JSX.Element => {
  const { tasks, getTask } = useTasks();
  const { links } = useLinks();

  const [selectedTasks, setSelectedTasks] = useState<Record<Task['id'], boolean>>({});
  const [selectedLinks, setSelectedLinks] = useState<Record<Link['id'], boolean>>({});

  useEffect(() => {
    const selectedIds = Object.keys(selectedTasks) as Task['id'][];
    const existingTasks = selectedIds.filter((taskId) => !!getTask(taskId));
    const newSelectedTasks: Record<Task['id'], boolean> = {};
    for (const taskId of existingTasks) {
      newSelectedTasks[taskId] = true;
    }
    // setSelectedTasks(newSelectedTasks);
  }, [tasks, getTask, selectedTasks]);

  // useEffect(() => {
  //   const selectedIds = Object.keys(selectedLinks) as Link['id'][];
  //   const existingLinkss = selectedIds.filter((linkId) => !!getLink(linkId));
  //   const newSelectedLinks: Record<Link['id'], boolean> = {};
  //   for (const linkId of existingLinkss) {
  //     newSelectedLinks[linkId] = true;
  //   }
  //   // setSelectedLinks(newSelectedLinks);
  // }, [links, getLink, selectedLinks]);

  const selectTasks: SelectionCtx['selectTasks'] = (taskIds = []) => {
    const selectedTasks: Record<Task['id'], boolean> = {};
    for (const taskId of taskIds) {
      selectedTasks[taskId] = true;
    }
    setSelectedTasks(selectedTasks);
    // TODO toggle
    // setSelectedTasks([
    //   ...selectedTasks,
    //   ...taskIds.filter((taskId) => !selectedTasks.includes(taskId)),
    // ]);
  };
  const isTaskSelected: SelectionCtx['isTaskSelected'] = (taskId) => {
    return !!selectedTasks[taskId];
  };
  const deselectTasks: SelectionCtx['deselectTasks'] = () => {};
  const selectLinks: SelectionCtx['selectLinks'] = () => {};
  const deselectLinks: SelectionCtx['deselectLinks'] = () => {};
  return (
    <SelectionContext.Provider
      value={{
        tasks: Object.keys(selectedTasks),
        links: Object.keys(selectedLinks),
        selectTasks,
        deselectTasks,
        isTaskSelected,
        selectLinks,
        deselectLinks,
      }}
    >
      {children}
    </SelectionContext.Provider>
  );
};

export const useSelection = () => useContext(SelectionContext);
