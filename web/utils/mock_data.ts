import { Filter, LinkDTO, TaskDTO, UserDTO, Workspace } from 'unravel-common';

export const mockData: {
  workspace: Workspace;
  members: UserDTO[];
  tasks: TaskDTO[];
  links: LinkDTO[];
  filters: Filter[];
} = {
  workspace: {
    id: 'workspace-0',
    title: 'Test workspace title',
    description: 'Some workspace description',
    code: 'UNR',
    fieldDescriptors: [
      {
        id: 'field-desc-0',
        type: 'text',
        name: 'Some text',
      },
      {
        id: 'field-desc-1',
        type: 'select',
        isMultiselect: false,
        name: 'Priority',
        options: [
          {
            id: 'desc-1-val-0',
            label: 'Urgent',
            color: 'white',
            bgColor: '#CD4246',
            border: '#CD4246',
          },
          {
            id: 'desc-1-val-1',
            label: 'High',
            color: '#C87619',
            bgColor: 'white',
            border: '#C87619',
          },
          {
            id: 'desc-1-val-2',
            label: 'Medium',
            color: '#215DB0',
            bgColor: 'white',
            border: '#215DB0',
          },
          {
            id: 'desc-1-val-3',
            label: 'Low',
            color: '#1C6E42',
            bgColor: 'white',
            border: '#1C6E42',
          },
        ],
      },
      {
        id: 'field-desc-2',
        type: 'select',
        isMultiselect: true,
        name: 'Workstream',
        options: [
          {
            id: 'desc-2-val-0',
            label: 'Frontend',
            color: 'rgb(87, 96, 106)',
            bgColor: 'rgb(246, 248, 250)',
            border: 'rgb(208, 215, 222)',
          },
          {
            id: 'desc-2-val-1',
            label: 'Backend',
            color: 'rgb(87, 96, 106)',
            bgColor: 'rgb(246, 248, 250)',
            border: 'rgb(208, 215, 222)',
          },
          {
            id: 'desc-2-val-2',
            label: 'Design',
            color: 'rgb(87, 96, 106)',
            bgColor: 'rgb(246, 248, 250)',
            border: 'rgb(208, 215, 222)',
          },
        ],
      },
      {
        id: 'field-desc-3',
        type: 'select',
        isMultiselect: false,
        name: 'Status',
        options: [
          {
            id: 'desc-3-val-0',
            label: 'To Do',
            color: 'rgb(87, 96, 106)',
            bgColor: 'white',
            border: 'rgb(208, 215, 222)',
          },
          {
            id: 'desc-3-val-1',
            label: 'In Progress',
            color: '#C87619',
            bgColor: 'white',
            border: '#C87619',
          },
          {
            id: 'desc-3-val-2',
            label: 'In Review',
            color: '#215DB0',
            bgColor: 'white',
            border: '#215DB0',
          },
          {
            id: 'desc-3-val-3',
            label: 'QA',
            color: '#1C6E42',
            bgColor: 'white',
            border: '#1C6E42',
          },
          {
            id: 'desc-3-val-4',
            label: 'Done',
            color: '#1C6E42',
            bgColor: 'white',
            border: '#1C6E42',
          },
          {
            id: 'desc-3-val-5',
            label: 'Blocked',
            color: '#CD4246',
            bgColor: 'white',
            border: '#CD4246',
          },
        ],
      },
      {
        id: 'field-desc-4',
        type: 'people',
        isMultiselect: true,
        name: 'Assignee',
      },
    ],
  },
  members: [
    {
      id: 'user-0',
      displayName: 'Unfold Research',
      avatar: 'https://avatars.githubusercontent.com/u/70808342?s=200&v=4',
    },
    {
      id: 'user-1',
      displayName: 'abstractalgo',
      avatar: 'https://avatars.githubusercontent.com/u/1355455?v=4',
    },
    {
      id: 'user-2',
      displayName: 'Nodebook',
      avatar: 'https://nodebook.io/logo.png',
    },
  ],
  filters: [
    {
      id: 'filter-0',
      isPinned: true,
      label: 'Acq & Eng (current sprint)',
      condition: {
        descId: 'field-desc-0',
        condition: 'da',
      },
    },
  ],
  tasks: [
    {
      id: 'task-0',
      title: 'Task 0',
      code: 0,
      createdBy: 'user-0',
      createdAt: Date.now(),
      x: 0,
      y: 0,
      workspaceId: `workspace-0`,
      fields: [],
    },
    {
      id: 'task-1',
      title: 'Task 1',
      code: 1,
      createdBy: 'user-1',
      createdAt: Date.now(),
      x: 0,
      y: 0,
      workspaceId: `workspace-0`,
      fields: [
        {
          fieldDescriptorId: 'field-desc-0',
          value: ['neki tekst'],
        },
        {
          fieldDescriptorId: 'field-desc-1',
          value: ['desc-1-val-1'],
        },
        {
          fieldDescriptorId: 'field-desc-2',
          value: ['desc-2-val-1', 'desc-2-val-2'],
        },
      ],
    },
    {
      id: 'task-2',
      title: 'Task 2',
      code: 2,
      createdBy: 'user-2',
      createdAt: Date.now(),
      x: 0,
      y: 0,

      workspaceId: `workspace-0`,      fields: [
        {
          fieldDescriptorId: 'field-desc-0',
          value: ['neki drugi tekst'],
        },
        {
          fieldDescriptorId: 'field-desc-1',
          value: ['desc-1-val-2'],
        },
        {
          fieldDescriptorId: 'field-desc-2',
          value: ['desc-2-val-0', 'desc-2-val-1', 'desc-2-val-2'],
        },
      ],
    },
    // second project
    {
      id: 'task-3',
      title: 'Task 3',
      code: 3,
      createdBy: 'user-2',
      createdAt: Date.now(),
      x: 100,
      y: 200,

      workspaceId: `workspace-0`,
      fields: [],    },
    {
      id: 'task-5',
      title: 'Task 5',
      code: 5,
      createdBy: 'user-2',
      createdAt: Date.now(),
      x: 0,
      y: 0,

      workspaceId: `workspace-0`,
      fields: [
        {          fieldDescriptorId: 'field-desc-0',
          value: ['neki peti tekst'],
        },
        {
          fieldDescriptorId: 'field-desc-1',
          value: ['desc-1-val-0'],
        },
        {
          fieldDescriptorId: 'field-desc-2',
          value: ['desc-2-val-0'],
        },
      ],
    },
    // third project
    {
      id: 'task-4',
      title: 'Task 4',
      code: 4,
      createdBy: 'user-1',
      createdAt: Date.now(),
      x: -600,
      y: 350,

      workspaceId: `workspace-0`,
      fields: [],
    },
    {      id: 'task-6',
      title: 'Task 6',
      code: 6,
      createdBy: 'user-2',
      createdAt: Date.now(),
      x: 0,
      y: 0,

      workspaceId: `workspace-0`,
      fields: [
        {
          fieldDescriptorId: 'field-desc-0',
          value: ['neki sesti tekst'],        },
        {
          fieldDescriptorId: 'field-desc-1',
          value: ['desc-1-val-2'],
        },
        {
          fieldDescriptorId: 'field-desc-2',
          value: ['desc-2-val-0', 'desc-2-val-1', 'desc-2-val-2'],
        },
      ],
    },
    {
      id: 'task-7',
      title: 'Task 7',
      code: 7,
      createdBy: 'user-2',
      createdAt: Date.now(),
      x: 0,
      y: 0,

      workspaceId: `workspace-0`,
      fields: [
        {
          fieldDescriptorId: 'field-desc-0',
          value: ['sedmi tekst'],
        },        {
          fieldDescriptorId: 'field-desc-1',
          value: ['desc-1-val-2'],
        },
        {
          fieldDescriptorId: 'field-desc-2',
          value: ['desc-2-val-0', 'desc-2-val-1', 'desc-2-val-2'],
        },
      ],
    },
    {
      id: 'task-8',
      title: 'Task 8',
      code: 8,
      createdBy: 'user-1',
      createdAt: Date.now(),
      x: 0,
      y: 0,

      workspaceId: `workspace-0`,
      fields: [
        {
          fieldDescriptorId: 'field-desc-0',
          value: ['osmi tekst'],
        },
        {          fieldDescriptorId: 'field-desc-1',
          value: ['desc-1-val-0'],
        },
        {
          fieldDescriptorId: 'field-desc-2',
          value: ['desc-2-val-1', 'desc-2-val-2'],
        },
      ],
    },
    // fourth
    {
      id: 'task-10',
      title: 'Task 10',
      code: 10,
      createdBy: 'user-2',
      createdAt: Date.now(),
      x: 200,
      y: 550,

      workspaceId: `workspace-0`,
      fields: [],
    },
  ],
  links: [
    // first project
    {
      id: 'link-0',      type: 'child',
      source: 'task-0',
      target: 'task-1',
      createdBy: 'user-0',
      createdAt: Date.now(),
    },
    {
      id: 'link-1',
      type: 'child',
      source: 'task-0',
      target: 'task-2',
      createdBy: 'user-0',
      createdAt: Date.now(),
    },
    // second project
    {
      id: 'link-2',
      type: 'child',
      source: 'task-3',
      target: 'task-5',
      createdBy: 'user-0',
      createdAt: Date.now(),
    },
    // third project
    {
      id: 'link-3',
      type: 'child',
      source: 'task-4',
      target: 'task-6',
      createdBy: 'user-0',
      createdAt: Date.now(),
    },
    {
      id: 'link-4',
      type: 'child',
      source: 'task-4',
      target: 'task-7',
      createdBy: 'user-0',
      createdAt: Date.now(),
    },
    {
      id: 'link-5',
      type: 'child',
      source: 'task-4',
      target: 'task-8',
      createdBy: 'user-0',
      createdAt: Date.now(),
    },
    {
      id: 'link-6',
      type: 'blocks',
      source: 'task-6',
      target: 'task-5',
      createdBy: 'user-0',
      createdAt: Date.now(),
    },
    {
      id: 'link-7',
      type: 'blocks',
      source: 'task-2',
      target: 'task-5',
      createdBy: 'user-0',
      createdAt: Date.now(),
    },
    {
      id: 'link-8',
      type: 'blocks',
      source: 'task-5',
      target: 'task-10',
      createdBy: 'user-0',
      createdAt: Date.now(),
    },
  ],
};

export const mockDataRnd: {
  workspace: Workspace;
  members: UserDTO[];
  tasks: TaskDTO[];
  links: LinkDTO[];
  filters: Filter[];
} = {
  workspace: mockData.workspace,
  filters: [],
  members: mockData.members,
  tasks: Array.from({length: 1000}).map((_,idx)=>({
    code: idx+1,
    id: `task-${idx}`,
    title: `Task ${idx+1}`,
    x: Math.random()*5_000,
    y: Math.random()*5_000,
    workspaceId: mockData.workspace.id,
    fields: [],
    createdAt: Date.now(),
    createdBy: 'user-0',
  })),
  links: [
    ...Array.from({length: 900}).map((_,idx) => ({
      id: `link-${idx}`,
      type: 'child' as LinkDTO['type'],
      source: `task-${Math.floor(Math.random()*100)}`,
      target: `task-${idx+100}`,
      createdBy: 'user-0',
      createdAt: Date.now(),
    })),
    ...Array.from({length: 200}).map((_,idx) => ({
      id: `link-${idx+1000}`,
      type: 'relates' as LinkDTO['type'],
      source: `task-${Math.floor(Math.random()*1000)}`,
      target: `task-${Math.floor(Math.random()*1000)}`,
      createdBy: 'user-0',
      createdAt: Date.now(),
    })),
  ]
}