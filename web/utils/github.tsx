import { Octokit } from '@octokit/rest';

const client = new Octokit({
  auth: process.env.GITHUB_PAT,
});

export const getAllIssues = async () => {
  const res = await client.paginate(client.issues.listForRepo, {
    owner: 'UnfoldResearch',
    repo: 'unfoldresearch',
    per_page: 100,
    state: 'open', // "all"
  });
  // const res = await client.issues.listForRepo();

  const issues = res;
  const openIssues = issues.filter((i) => i.state === 'open');
  console.log(`${openIssues.length}/${issues.length} open`);
  console.log(issues[0]);
  return openIssues;
};
