export const clamp = (value: number, min: number, max: number): number => {
  const _val = Math.max(min, value);
  const _val_ = Math.min(_val, max);
  return _val_;
};
