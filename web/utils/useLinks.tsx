import { createContext, ReactNode, RefObject, useContext, useEffect, useRef, useState } from 'react';
import { LinkDTO } from 'unravel-common';
import { Task } from './useTasks';

export type Link = Pick<LinkDTO, 'id' | 'type'> & {
  sourceTask: Task;
  targetTask: Task;
  ref: RefObject<SVGPathElement>;
};

type LinksCtx = {
  links: Link[];
  createLink: (options: Pick<Link, 'sourceTask' | 'targetTask' | 'type'>) => Link;
  setLinks: (links: Link[]) => void;
};

const LinksContext = createContext<LinksCtx>({
  links: [],
  // @ts-ignore
  createLink: () => {},
  setLinks: () => {},
});

export const LinksProvider = ({ children }: { children: ReactNode }): JSX.Element => {
  const [links, setLinks] = useState<Link[]>([]);

  const createLink: LinksCtx['createLink'] = ({ sourceTask, targetTask, type }) => {
    const newLink: Link = {
      id: `link-${Math.floor(Math.random() * 10_000)}`,
      sourceTask,
      targetTask,
      type,
      ref: {
        current: null,
      },
    };
    sourceTask.links.push(newLink);
    targetTask.links.push(newLink);

    setLinks([...links, newLink]);

    return newLink;
  };

  return (
    <LinksContext.Provider
      value={{
        links,
        createLink,
        setLinks,
      }}
    >
      {children}
    </LinksContext.Provider>
  );
};

export const useLinks = () => useContext(LinksContext);
