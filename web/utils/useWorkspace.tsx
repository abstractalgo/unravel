import { createContext, ReactNode, useContext, useState } from 'react';
import { Workspace } from 'unravel-common';

type WorkspaceCtx = {
  workspace: Workspace | null;
  setWorkspace: (workspace: Workspace) => void;
};

const WorkspaceContext = createContext<WorkspaceCtx>({
  workspace: null,
  setWorkspace: () => {},
});

export const WorkspaceProvider = ({ children }: { children: ReactNode }): JSX.Element => {
  const [workspace, setWorkspace] = useState<Workspace | null>(null);

  return (
    <WorkspaceContext.Provider
      value={{
        workspace,
        setWorkspace,
      }}
    >
      {children}
    </WorkspaceContext.Provider>
  );
};

export const useWorkspace = () => useContext(WorkspaceContext);
