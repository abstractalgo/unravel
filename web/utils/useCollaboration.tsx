import { createContext, ReactNode, useContext, useState } from 'react';
import { UserDTO } from 'unravel-common';

export type User = UserDTO;

type CollaborationCtx = {
  collaborators: User[];
  setCollaborators: (collaborators: User[]) => void;
};

const CollaborationContext = createContext<CollaborationCtx>({
  collaborators: [],
  setCollaborators: () => {},
});

export const CollaborationProvider = ({ children }: { children: ReactNode }): JSX.Element => {
  const [collaborators, setCollaborators] = useState<User[]>([]);

  return (
    <CollaborationContext.Provider
      value={{
        collaborators,
        setCollaborators,
      }}
    >
      {children}
    </CollaborationContext.Provider>
  );
};

export const useCollaboration = () => useContext(CollaborationContext);
