import { createContext, ReactNode, useContext, useMemo, useState } from 'react';
import { Comment } from 'unravel-common';
import { Task } from './useTasks';

type CommentsCtx = {
  comments: Comment[];
  setComments: (comments: Comment[]) => void;
};

const CommentsContext = createContext<CommentsCtx>({
  comments: [],
  setComments: () => {},
});

const useCommentsData = (): CommentsCtx => {
  const [comments, setComments] = useState<Comment[]>([]);

  return {
    comments,
    setComments,
  };
};

export const CommentsProvider = ({ children }: { children: ReactNode }): JSX.Element => {
  const commentsCtx = useCommentsData();
  return <CommentsContext.Provider value={commentsCtx}>{children}</CommentsContext.Provider>;
};

type CommentsHook = {
  comments: Comment[];
  addComment: (content: string) => void;
  removeComment: (commentId: Comment['id']) => void;
  updateComment: (commentId: Comment['id'], content: string) => void;
};

export const useComments = (taskId: Task['id']): CommentsHook => {
  const ctx = useContext(CommentsContext);

  const comments = useMemo(() => ctx.comments.filter((comm) => comm.id === taskId), [taskId, ctx.comments]);

  const addComment: CommentsHook['addComment'] = (content) => {
    ctx.setComments([
      ...ctx.comments,
      // {
      //   id: `comm-${taskId}-${Math.floor(Math.random() * 1000)}`,
      //   authorId: "user-0",
      //   content,
      //   item: {
      //     type: 'task',
      //     id: taskId,
      //   }
      // },
    ]);
  };
  const removeComment: CommentsHook['removeComment'] = (commentId) => {
    ctx.setComments(ctx.comments.filter((comm) => comm.id !== commentId));
  };
  const updateComment: CommentsHook['updateComment'] = (commentId, content) => {
    const idx = ctx.comments.findIndex((comm) => comm.id === commentId);
    const newComments = [...ctx.comments];
    newComments[idx] = {
      ...newComments[idx],
      content,
    };
    ctx.setComments(newComments);
  };

  return {
    comments,
    addComment,
    removeComment,
    updateComment,
  };
};
