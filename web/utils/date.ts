export const formatTimeDiff = (oldDatetime: number, newDatetime: number = Date.now()): string => {
  const seconds = (newDatetime - oldDatetime) / 1000;
  const hours = seconds / (60 * 60);

  const days = seconds / (24 * 60 * 60);

  const fullDays = Math.floor(days);
  const fullHours = Math.floor((seconds - fullDays * 24 * 60 * 60) / (60 * 60));
  const minutes = Math.floor(((seconds % 60) * 60) / 60);

  if (days >= 1) {
    return `${fullDays}d ${fullHours}h ${minutes} ago`;
  } else if (hours > 1) {
    return `${fullHours}h ${minutes}m ago`;
  } else {
    return `${minutes}m ago`;
  }
};
