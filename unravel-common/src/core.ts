export type CUID = string;
type URL = string;

export type Workspace = {
  id: CUID;
  title: string;
  description: string;
  code: string;
  fieldDescriptors: FieldDesc[];
};

export type UserDTO = {
  id: CUID;
  displayName: string;
  avatar?: URL;
};

export type FieldDesc = {
  id: CUID;
  name: string;
} & (CustomSelectFieldDesc | TextFieldDesc | PeopleSelectFieldDesc);

export const FieldTypeOptions = ['select', 'text', 'people'] as const;

export type FieldType = typeof FieldTypeOptions[number];

export type CustomSelectFieldDesc = {
  type: typeof FieldTypeOptions[0];
  options: {
    id: CUID;
    label: string;
    color: string;
    bgColor: string;
    border: string;
  }[];
  isMultiselect: boolean;
};

export type TextFieldDesc = {
  type: typeof FieldTypeOptions[1];
};

export type PeopleSelectFieldDesc = {
  type: typeof FieldTypeOptions[2];
  isMultiselect: boolean;
};

export type FieldValue = string[]; // can always be string or string[]

export type TaskDTO = {
  id: CUID;
  workspaceId: Workspace['id'];
  createdBy: UserDTO['id'];
  createdAt: number;
  title: string;
  code: number; // project-specific unique id, e.g. UNR-34
  fields: {
    fieldDescriptorId: FieldDesc['id'];
    value: FieldValue;
  }[];
  // project
  x: number;
  y: number;
  width?: number;
  height?: number;
};

export type LinkDTO = {
  id: CUID;
  type: 'child' | 'depends-on' | 'blocks' | 'blocked-by' | 'relates';
  source: TaskDTO['id'];
  target: TaskDTO['id'];
  createdAt: number;
  createdBy: UserDTO['id'];
};

// -- comments -----------------------------------------------------------------

export const ReactionOptions = [
  'thumbs-up',
  'thumbs-down',
  'heart',
  'rocket',
  'eyes',
  'ta-da',
  'smile',
  'sad',
] as const;
export type Reaction = typeof ReactionOptions[number];

export type Comment = {
  id: CUID;
  content: string;
  authorId: UserDTO['id'];
  createdAt: number;
  updatedAt: number | null;
  reactions: Partial<Record<Reaction, number>>;
};

// -- filters ------------------------------------------------------------------

export type Condition = {
  descId: FieldDesc['id'];
  condition: unknown;
};

export type ConditionGroup = {
  left: Condition | ConditionGroup;
  right: Condition | ConditionGroup;
  op: 'and' | 'or' | 'minus' | 'spread';
};

export type Filter = {
  id: CUID;
  label: string;
  isPinned: boolean;
  condition: /* ConditionGroup |  */ Condition; // TODO recursive
};

// -- events -------------------------------------------------------------------

export type Event = {
  id: string;
  type: 'created';
  createdAt: number;
};

export type EventType = Event['type'];
