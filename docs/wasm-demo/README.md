### Getting started

https://developer.mozilla.org/en-US/docs/WebAssembly/Rust_to_wasm

- `cargo install wasm-pack`
- `cargo new --lib <name>`
- `wasm-pack build --target web`

### Examples

Examples of using `wasm-bindgen`, `js-sys`, and `web-sys`.

> `wasm-bindgen`: https://crates.io/crates/wasm-bindgen
>
> Easy support for interacting between JS and Rust.

> `js-sys`: https://crates.io/crates/js-sys
>
> Bindings for all JS global objects and functions in all JS environments like Node js and browsers, built on `#[wasm_bindgen]` using the `wasm-bindgen` crate.

> `web-sys`: https://crates.io/crates/web-sys
>
> Bindings for all Web APIs, a procedurally generated crate from WebIDL.

https://rustwasm.github.io/wasm-bindgen/examples/
