use wasm_bindgen::prelude::*;
use web_sys::{Document, Window};

#[wasm_bindgen]
extern "C" {
    pub fn alert(s: &str);

    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

#[wasm_bindgen]
pub fn greet(name: &str) {
    log("fuck yeah")
    // alert(&format!("Hello, {}!", name));
}

#[wasm_bindgen]
pub fn dom_greet(name: &str) {
    // Use `web_sys`'s global `window` function to get a handle on the global
    // window object.
    let window = web_sys::window().unwrap();
    let document = window.document().unwrap();
    let body = document.body().unwrap();

    // Manufacture the element we're gonna append
    let val = document.create_element("p").unwrap();
    let mut greeting = "Hello from Rust!".to_owned();
    greeting.push_str(name);
    val.set_text_content(Some(&greeting));

    body.append_child(&val);
}
